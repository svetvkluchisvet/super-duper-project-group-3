## Тема проекта
Крутой сайт погоды.

## Tech Stack

* Framework React ⚛️
* Style preprocessor: CSS/Less/Sass

## Название проекта:

* MyWeather
* WEATHER.io
* WEATHERIO
* True Weather
* We At Her
* Heaven
* ...

## ФиЧи.

1. Localization.
2. Dark/Light Theme.
3. Weather `API` integration
4. Maps
5. Location API. Internalization.
6. Разные виджеты под погодные условия.
7. ...
