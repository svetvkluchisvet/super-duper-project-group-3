import { FC, memo } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import AirPolutionContent from "./components/air-pollution-page-content/AirPollutionPageContent";
import CardComponent from "./components/cards/card.component";
import DailyForecastCard from "./components/daily-forecast-card/DailyForecastCard";
import Page from "./components/page/Page";
import styles from "./components/page/Page.module.css";
import SuggestionCardContent from "./components/suggestion-card/SuggestionCard";
import SunAndMoonCardContent from "./components/sun-moon-card/SunAndMoonCard";
import WeekForecast from "./components/week-forecast/week-forecast";
import WeatherForecastContainer from "./components/weather-forecast-container/WeatherForecastContainer";
import { AppDataContextModel, useAppDataContext } from "./context/app-data.provider";
import WeatherMapComponent from "./components/weather-map/weather-map.component";

const App: FC = () => {

  const { darkMode } = useAppDataContext() as AppDataContextModel;

  return (
    <BrowserRouter>
      <Routes>
        <Route
          path={"/"}
          element={
            <div className={darkMode ? "dark" : "light"}>
              <Page>
                <WeatherForecastContainer>
                  <DailyForecastCard timeOfDayIndex={0} type="morning" />
                  <DailyForecastCard timeOfDayIndex={1} type="afternoon" />
                  <DailyForecastCard timeOfDayIndex={2} type="evening" />
                  <DailyForecastCard timeOfDayIndex={3} type="night" />
                </WeatherForecastContainer>
  
                <div className={styles.cardContainer}>
                  <CardComponent>
                    <SuggestionCardContent />
                  </CardComponent>
                  <CardComponent>
                    <SunAndMoonCardContent />
                  </CardComponent>
                </div>
                <WeatherMapComponent/>
                <WeatherForecastContainer>
                  <WeekForecast />
                </WeatherForecastContainer>
              </Page>
            </div>
          }
        />
        <Route
          path={"/air-pollution"}
          element={
            <div className={darkMode ? "dark" : "light"}>
            <Page>
              <AirPolutionContent />
            </Page>
            </div>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};

export default memo(App);
