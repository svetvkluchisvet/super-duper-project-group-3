import { useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { AppDataContextModel, useAppDataContext } from "../../context/app-data.provider";
import { useGetCurrentWeatherQuery } from "../../features/currentWeatherApi/currentWeatherApi";
import { locationSliceSelectors } from "../../features/geocodingSlice/geocodingSlice";
import { useTypedSelector } from "../../features/selector/selector";


export type MainWeatherData = {
  city?: string;
  temp?: number;
  feelsLike?: number;
  windDirection?: string;
  windSpeed?: number;
  icon?: string;
};

export const useMainWeatherData = (): {
  widgetContent: MainWeatherData;
  isLoading: boolean;
} => {
  const lat = useSelector((state: RootState) => state.locationSlice.lat);
  const lon = useSelector((state: RootState) => state.locationSlice.lon);
  const { language } = useAppDataContext() as AppDataContextModel;

  const { data, isLoading } = useGetCurrentWeatherQuery({ lat, lon, language });

  const widgetContent = {
    city: data?.name,
    temp: data?.main.temp,
    feelsLike: data?.main.feels_like,
    windDirection: data?.wind.deg,
    windSpeed: data?.wind.speed,
    icon: data?.weather[0].icon,
  };

  return { widgetContent, isLoading };
};
