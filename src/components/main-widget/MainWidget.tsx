import {FC, memo} from "react";
import {useTranslation} from "react-i18next";
import { AppDataContextModel, useAppDataContext } from "../../context/app-data.provider";
import MetricDescription from "../metric-description/MetricDescription";
import PreloaderWithClouds from "../preloader-with-clouds/PreloaderWithClouds";
import styles from "./MainWidget.module.css";
import {useMainWeatherData} from "./use-main-weather-data";



const MainWidget: FC = () => {
    const { language } = useAppDataContext() as AppDataContextModel;


    const currentDate = new Date().toLocaleDateString(language, {
      month: "long",
      day: "numeric",
    });
    const {t} = useTranslation();
    const {widgetContent, isLoading} = useMainWeatherData();

    if (isLoading) {
        return (
            <div className={styles.widgetContainer}>
                <PreloaderWithClouds/>
            </div>
        )
    }

    const temp = Math.round(Number(widgetContent.temp));
    const feelsLike = Math.round(Number(widgetContent.feelsLike));
    const direction = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N'];
    const windDirection = direction[Math.round(Number(widgetContent.windDirection) / 45)];

    return (
        <div className={styles.widgetContainer}>
            <p>{currentDate}</p>
            <p className={styles.city}>{widgetContent.city}</p>
            <img src={`http://openweathermap.org/img/wn/${widgetContent.icon}@2x.png`} alt="weather icon"
                 className={styles.weatherIcon}/>
            <p className={styles.temperature}>{temp}°C</p>
            <div className={styles.fLW}>
                <MetricDescription metricName={t('main_widget.feels_like_title')} metricValue={`${feelsLike}°C`}
                                   type="medium"/>
                <MetricDescription metricName={t('main_widget.wind_title')}
                                   metricValue={`${widgetContent.windSpeed} ${t('main_widget.wind_units_of_measurement')}, ${windDirection}`}
                                   type="medium"/>
            </div>
        </div>
    );
}

export default memo(MainWidget);