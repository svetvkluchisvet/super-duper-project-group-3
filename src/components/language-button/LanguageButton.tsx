import { faGlobe } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Popover } from "antd";
import { FC, memo } from "react";
import {
  AppDataContextModel,
  useAppDataContext,
} from "../../context/app-data.provider";
import i18n from "../../i18next";
import styles from "./LanguageButton.module.css";

const LanguageButton: FC = () => {
  const { setLanguage } = useAppDataContext() as AppDataContextModel;
  return (
    <Popover
      placement="left"
      content={
        <ul className={styles.languageList}>
          <li className={styles.languageItem}>
            <button
              className={styles.button}
              onClick={() => {
                setLanguage("en");
                i18n.changeLanguage("en");
              }}
            >
              English
            </button>
          </li>
          <li className={styles.languageItem}>
            <button
              className={styles.button}
              onClick={() => {
                i18n.changeLanguage("de");
                setLanguage("de");
              }}
            >
              German
            </button>
          </li>
          <li className={styles.languageItem}>
            <button
              className={styles.button}
              onClick={() => {
                i18n.changeLanguage("ru");
                setLanguage("ru");
              }}
            >
              Russian
            </button>
          </li>
        </ul>
      }
      trigger="hover"
    >
      <div className={styles.language}>
        <FontAwesomeIcon
          icon={faGlobe}
          fontSize={30}
          color={"var(--font-color)"}
          fixedWidth
        />
      </div>
    </Popover>
  );
};

export default memo(LanguageButton);
