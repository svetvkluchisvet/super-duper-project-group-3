import { FC, memo, PropsWithChildren } from 'react';
import styles from './RightAside.module.css';

interface RightAsideProps {}

const RightAside: FC<PropsWithChildren<RightAsideProps>> = ({children}) => {
    return (
        <aside className={styles.aside}>
            {children}
        </aside>
    );
}

export default memo(RightAside);
