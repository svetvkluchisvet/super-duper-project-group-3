import { useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { AppDataContextModel, useAppDataContext } from "../../context/app-data.provider";
import { useGetCurrentWeatherQuery } from "../../features/currentWeatherApi/currentWeatherApi";


export type SuggestionWeatherData = {
  temp?: number;
  pressure?: number;
  feelsLike?: number;
  windSpeed?: number;
  weather?: string;
};

export const useSuggestionWeatherData = (): {suggestionCardContent: SuggestionWeatherData, isLoading: boolean} => {
    const lat = useSelector((state: RootState) => state.locationSlice.lat);
    const lon = useSelector((state: RootState) => state.locationSlice.lon);
    const { language } = useAppDataContext() as AppDataContextModel;

  const { data, isLoading } = useGetCurrentWeatherQuery({ lat, lon, language });

  const suggestionCardContent = {
    temp: data?.main.temp,
    feelsLike: data?.main.feels_like,
    windSpeed: data?.wind.speed,
    pressure: data?.main.pressure,
  };

  return { suggestionCardContent, isLoading };
};
