import { FC, memo, useCallback } from "react";
import styles from "../suggestion-card/SuggestionCard.module.css";
import { useSuggestionWeatherData } from "./use-suggestion-data";
import { useTranslation } from "react-i18next";
import confetti from 'canvas-confetti';

const SuggestionCard: FC = () => {
    const { t } = useTranslation();
    const { suggestionCardContent } = useSuggestionWeatherData();

    const feelsLike = Math.round(Number(suggestionCardContent.feelsLike)) ;//- 273;
    const windSpeed = Number(suggestionCardContent.windSpeed);
    const onClick = useCallback(() => {
        confetti({
            particleCount: 150,
            spread: 60,
            origin: {x: 0.5, y: 1}
        });
    }, []);

    if (feelsLike < 0 && windSpeed > 7.5) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.freezing_cold")}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.you_should_dress_warmer")}
                </p>
                <div className={styles.modalContent}>
                    <div className={styles.firstBlock}>
                        <button className={styles.btn} onClick={onClick}>
                            {t("suggestion_card.thank_you")}
                        </button>
                    </div>
                </div>
            </div>
        );
    } else if (feelsLike < 0 && windSpeed <= 7.5) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.rather_cold")}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.maybe_dress_warmer")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else if (feelsLike >= 0 && feelsLike < 10 && windSpeed >= 7.5) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t('suggestion_card".chilly_is_not_it')}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.and_the_wind_is_not_weak")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else if (feelsLike >= 0 && feelsLike < 10 && windSpeed < 7.5) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>{t("suggestion_card.cool")}</h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.put_on_scarf")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else if (feelsLike >= 10 && feelsLike <= 15 && windSpeed >= 7.5) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.not_so_cold")}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.but_strong_wind_is_blowing")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else if (feelsLike >= 10 && feelsLike <= 15 && windSpeed < 7.5) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.seems_warm")}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.but_still_take_jacket")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else if (feelsLike >= 15 && feelsLike < 25) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.warmly_and_well")}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.have_good_day")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else if (feelsLike >= 25) {
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.wow_warm_out")}
                </h3>
                <p className={styles.suggestionDescript}>
                    {t("suggestion_card.use_sunscreen")}
                </p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
    } else
        return (
            <div className={styles.flipCard}>
                <h3 className={styles.suggestionTemp}>
                    {t("suggestion_card.nature_has_no_bad_weather")}
                </h3>
                <p className={styles.suggestionDescript}>{}</p>
                <div>
                    <button className={styles.btn} onClick={onClick}>
                        {t("suggestion_card.thank_you")}
                    </button>
                </div>
            </div>
        );
};

export default memo(SuggestionCard);
