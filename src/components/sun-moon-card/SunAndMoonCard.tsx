import { FC, memo } from "react";
import { useTranslation } from "react-i18next";
import MetricDescription from "../metric-description/MetricDescription";
import styles from "../sun-moon-card/SunAndMoon.module.css";
import { useSunMoonData } from "./use-sun-moon-data";


interface SunAndMoonCardContentProps {}

const SunAndMoonCardContent: FC<SunAndMoonCardContentProps> = () => {
  const { t } = useTranslation();
  const { spaceContent } = useSunMoonData();
  return (
    <div className={styles.flipCard}>
      <div className={styles.flipCardInner}>
        <div className={styles.flipCardFront}>
          <h3 className={styles.title}>{t('sun_moon_card.sun')}</h3>
          <MetricDescription metricName={t('sun_moon_card.sunrise')} metricValue={`${spaceContent?.sunrise}`} type="main" />
          <MetricDescription metricName={t('sun_moon_card.sunset')} metricValue={`${spaceContent?.sunset}`} type="main" />
        </div>
        <div className={styles.flipCardBack}>
          <h3 className={styles.title}>{t('sun_moon_card.moon')}</h3>
          <MetricDescription metricName={t('sun_moon_card.moonrise')} metricValue={`${spaceContent?.moonrise}`} type="main" />
          <MetricDescription metricName={t('sun_moon_card.moonset')} metricValue={`${spaceContent?.moonset}`} type="main" />
        </div>
      </div>
    </div>
  );
};

export default memo(SunAndMoonCardContent);
