import { useAppSelector } from "../../app/hooks";
import { useGetCurrentForecastQuery } from "../../features/currentForecastApi/currentForecastApi";
import moment from "moment";
import { RootState } from "../../app/store";
import { useSelector } from "react-redux";
import { AppDataContextModel, useAppDataContext } from "../../context/app-data.provider";


export type SunMoonData = {
  sunrise?: string;
  sunset?: string;
  moonrise?: string;
  moonset?: string;
};

const unixMillisToDate = (millis: number): Date => {
  return moment.unix(millis).toDate();
};

export const useSunMoonData = (): {
  spaceContent?: SunMoonData;
  isLoading: boolean;
} => {
  const lat = useSelector((state: RootState) => state.locationSlice.lat);
  const lon = useSelector((state: RootState) => state.locationSlice.lon);
  const { language } = useAppDataContext() as AppDataContextModel;

  const { data, isLoading } = useGetCurrentForecastQuery({
    lat,
    lon,
    language,
  });

  if (data) {
    const todayWeather = data?.daily[0];
    const spaceContent = {
      sunrise: unixMillisToDate(todayWeather?.sunrise).toLocaleString("ru", {
        hour: "numeric",
        timeZone: data.timezone,
        minute: "numeric",
      }),
      sunset: unixMillisToDate(todayWeather?.sunset).toLocaleString("ru", {
        hour: "numeric",
        timeZone: data.timezone,
        minute: "numeric",
      }),
      moonrise: unixMillisToDate(todayWeather?.moonrise).toLocaleString("ru", {
        hour: "numeric",
        timeZone: data.timezone,
        minute: "numeric",
      }),
      moonset: unixMillisToDate(todayWeather?.moonset).toLocaleString("ru", {
        hour: "numeric",
        timeZone: data.timezone,
        minute: "numeric",
      }),
    };
    return { spaceContent, isLoading };
  }
  return { isLoading };
};
