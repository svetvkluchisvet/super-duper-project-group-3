import {FC, memo} from 'react';
import styles from './Logo.module.css';
import LogoPic from '../logo-pic/LogoPic';

const Logo: FC = () => {
  return (
    <div className={styles.logoBackground}>
      <LogoPic/>
    </div>
  )
};

export default memo(Logo);
