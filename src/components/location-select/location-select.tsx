import { Select } from "antd";
import 'antd/dist/antd.css';
import { LabeledValue } from "antd/lib/select";
import { debounce } from "lodash";
import React, {
  FC,
  memo,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useAppDispatch } from "../../app/hooks";
import { locationSliceActions } from "../../features/geocodingSlice/geocodingSlice";
import { useCitiesInfo } from "./use-cities-info";
import styles from './location-select.module.css';

interface LocationSelectProps {}

const LocationSelect: FC<LocationSelectProps> = () => {
  const dispatch = useAppDispatch();
  const [searchValue, setSearchValue] = useState("");
  const [selectedCity, setSelectedCity] = useState<string>();
  const { citiesInfo, trigger, isLoading } = useCitiesInfo();

  useEffect(() => {  navigator.geolocation.getCurrentPosition(position => {
    dispatch(
      locationSliceActions.setLocationData({
        lat: position.coords.latitude,
        lon: position.coords.longitude,
      })
    );
   })
  }, []);


  const options: LabeledValue[] = useMemo(() => {
    return citiesInfo.map((it) => ({
      value: it.city,
      label: it.city,
      key: it.id.toString(),
    }));
  }, [citiesInfo]);

  useEffect(() => {
    if (selectedCity) {
      const cityInfo = citiesInfo.find((it) => it.city === selectedCity);
      if (cityInfo) {
        dispatch(
          locationSliceActions.setLocationData({
            city: cityInfo.name,
            lat: cityInfo.latitude,
            lon: cityInfo.longitude,
          })
        );
      }
    }
  }, [citiesInfo, dispatch, selectedCity]);

  const searchDebounce = useCallback(
    debounce((search: string) => trigger({ namePrefix: search }), 1001),
    [trigger]
  );

  const handleSearch = useCallback(
    (search: string) => {
      setSearchValue(search);
      searchDebounce(search);
    },
    [searchDebounce]
  );

  return (
    <Select
      showSearch
      className={styles.select}
      loading={isLoading}
      placeholder="Select a town"
      onChange={(city) => setSelectedCity(city)}
      searchValue={searchValue}
      onSearch={handleSearch}
      options={options}
    >
    </Select>
  );
};

export default memo(LocationSelect);
