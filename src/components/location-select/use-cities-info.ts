import {
  useGetCitiesQuery,
  useLazyGetCitiesQuery,
} from "../../features/geocodingApi/citiesApi";
import { CityInfo } from "../../types/cities-api.types";

export interface UseCitiesInfoResult {
  citiesInfo: CityInfo[];
  isLoading: boolean;
  trigger: (args: any) => any;
}
export const useCitiesInfo = (): UseCitiesInfoResult => {
  const [trigger, result, lastPromiseInfo] = useLazyGetCitiesQuery();
  return {
    citiesInfo: result.data ? result.data.data : [],
    isLoading: result.isLoading,
    trigger,
  };
};
