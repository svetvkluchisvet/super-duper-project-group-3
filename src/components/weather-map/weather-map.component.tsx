import { FC, memo, useEffect, useState } from "react";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  LayersControl,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { useSelector } from "react-redux";
import { RootState } from "../../app/store";
import L from "leaflet";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import PopupWeather from "./popup-wether";
import PreloaderWithClouds from "../preloader-with-clouds/PreloaderWithClouds";
import { useMainWeatherData } from "../main-widget/use-main-weather-data";
import { Admarker } from "./add-marker";
import {
  AppDataContextModel,
  useAppDataContext,
} from "../../context/app-data.provider";
import styles from "./WeatherMap.module.css";

const DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
});
L.Marker.prototype.options.icon = DefaultIcon;

const WeatherMapComponent: FC = () => {
  const latLon: [number, number] = [
    useSelector((state: RootState) => state.locationSlice.lat),
    useSelector((state: RootState) => state.locationSlice.lon),
  ];
  const { isLoading } = useMainWeatherData();
  const [isMapDark, setIsMapDark] = useState(false);
  const { darkMode } = useAppDataContext() as AppDataContextModel;

  function setTileLayer() {
    if (darkMode) {
      setIsMapDark(true);
    } else {
      setIsMapDark(false);
    }
  }

  useEffect(() => {
    setTileLayer();
  }, [darkMode]);

  function TileLayerDark() {
    return (
      <TileLayer
        attribution='&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png?apikey=5048c8053d0a4a28bf2cfbdbe4a715a7"
      />
    );
  }

  function TileLayerLight() {
    return (
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
    );
  }

   if (isLoading) {
     return <PreloaderWithClouds />;
   }

  return (
    <>
      <MapContainer
        fadeAnimation={true}
        zoomAnimation={true}
        attributionControl={false}
        center={latLon}
        zoom={9}
        scrollWheelZoom={true}
        className={styles.mapContainer}
      >
        {isMapDark ? <TileLayerDark/> : <TileLayerLight/>}
        <LayersControl position="topright">
          <LayersControl.Overlay name="Облачность">
            <TileLayer
              url={
                "https://tile.openweathermap.org/map/clouds_new/{z}/{x}/{y}.png?appid=e6ea861dc5f941af90e5a394268e8342"
              }
            ></TileLayer>
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Атмосферные осадки">
            <TileLayer
              url={
                "https://tile.openweathermap.org/map/precipitation_new/{z}/{x}/{y}.png?appid=e6ea861dc5f941af90e5a394268e8342"
              }
            ></TileLayer>
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Давление">
            <TileLayer
              url={
                "https://tile.openweathermap.org/map/pressure_new/{z}/{x}/{y}.png?appid=e6ea861dc5f941af90e5a394268e8342"
              }
            ></TileLayer>
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Ветер">
            <TileLayer
              url={
                "https://tile.openweathermap.org/map/wind_new/{z}/{x}/{y}.png?appid=e6ea861dc5f941af90e5a394268e8342"
              }
            ></TileLayer>
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Температура">
            <TileLayer
              url={
                "https://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=e6ea861dc5f941af90e5a394268e8342"
              }
            ></TileLayer>
          </LayersControl.Overlay>
        </LayersControl>
        <Marker position={latLon}>
          <Popup maxHeight={128} maxWidth={256}>
            <PopupWeather />
          </Popup>
        </Marker>
        <Admarker />
      </MapContainer>
    </>
  );
};

export default memo(WeatherMapComponent);
