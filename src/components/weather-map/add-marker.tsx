import L, { layerGroup, marker } from "leaflet";
import { useMapEvents } from "react-leaflet";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import { useAppDispatch } from "../../app/hooks";
import { locationSliceActions } from "../../features/geocodingSlice/geocodingSlice";

const DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
});
L.Marker.prototype.options.icon = DefaultIcon;

export function Admarker() {
  const dispatch = useAppDispatch();
  useMapEvents({
    click: (e) => {
      const { lat, lng } = e.latlng;
      const markerLayer = L.layerGroup();
      const marker = L.marker([lat, lng], { icon: DefaultIcon })
        .bindPopup("")
        .addTo(markerLayer);

      markerLayer.removeLayer(marker);

      dispatch(
        locationSliceActions.setLocationData({
          lat: e.latlng.lat,
          lon: e.latlng.lng,
        })
      );
    },
  });
  return null;
}