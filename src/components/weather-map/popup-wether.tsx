import { FC, memo } from "react";
import { useTranslation } from "react-i18next";
import { useMainWeatherData } from "../main-widget/use-main-weather-data";
import MetricDescription from "../metric-description/MetricDescription";
import PreloaderWithClouds from "../preloader-with-clouds/PreloaderWithClouds";
import styles from "./WeatherMap.module.css";



const currentDate = new Date().toLocaleDateString('en', {
  month: 'long',
  day: 'numeric'
})

const PopupWeather: FC = () => {
  const { t } = useTranslation();
  const { widgetContent, isLoading } = useMainWeatherData();

  if (isLoading) {
    return (
      <div className={styles.widgetContainer}>
        <PreloaderWithClouds />
      </div>
    )
  }

  const temp = Math.round(Number(widgetContent.temp));
  const feelsLike = Math.round(Number(widgetContent.feelsLike));
  const direction = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N'];
  const windDirection = direction[Math.round(Number(widgetContent.windDirection)/45)];
  
  return (
    <div className={styles.popupContainer}>
      <p>{currentDate}</p>
      <p className={styles.cityName}>{widgetContent.city}</p>
      <img src={`http://openweathermap.org/img/wn/${widgetContent.icon}@2x.png`} alt="weather icon" className={styles.weatherIcon}/>
      <p className={styles.popupTemperature}>{temp}°C</p>
      <MetricDescription metricName={t('main_widget.feels_like_title')} metricValue={`${feelsLike}°C`} type="medium" />
      <MetricDescription metricName={t('main_widget.wind_title')} metricValue={`${widgetContent.windSpeed} ${t('main_widget.wind_units_of_measurement')}, ${windDirection}`} type="medium" />
    </div>
  );
}

export default memo(PopupWeather);