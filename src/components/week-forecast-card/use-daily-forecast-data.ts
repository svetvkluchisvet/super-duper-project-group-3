import { useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { useGetCurrentForecastQuery } from "../../features/currentForecastApi/currentForecastApi";
import { AppDataContextModel, useAppDataContext } from "../../context/app-data.provider";


export type WeekForecastData = {
  clouds?: number;
  dt?: string;
  feelsLike?: number;
  icon?: string;
  mainWeatherType?: string;
  pressure?: number;
  probabilityOfPrecipitation?: number;
  rain?: number;
  temp?: number;
  uvi?: number;
  weatherDescription?: string;
  windSpeed?: number;
};

export const useDailyForecastData = (): { weekForecast: WeekForecastData[], isLoading: boolean } => {
  const lat = useSelector((state: RootState) => state.locationSlice.lat);
  const lon = useSelector((state: RootState) => state.locationSlice.lon);
  const { language } = useAppDataContext() as AppDataContextModel;

  const { data, isLoading } = useGetCurrentForecastQuery({
    lat,
    lon,
    language,
  });

  const weekForecast = [];

  const currentDate = new Date();

  for (let i = 0; i < 8; ++i) {
    weekForecast.push({
      clouds: data?.daily[i].clouds,
      dt: currentDate.toLocaleDateString(language, {
        month: "long",
        day: "numeric",
      }),
      feelsLike: Math.round(Number(data?.daily[i].feels_like.day)) || 0,
      icon: data?.daily[i].weather[0].icon,
      mainWeatherType: data?.daily[i].weather[0].main,
      pressure: data?.daily[i].pressure,
      probabilityOfPrecipitation: Number(data?.daily[i].pop) * 100 || 0,
      rain: data?.daily[i].rain || 0,
      temp: Math.round(Number(data?.daily[i].temp.day)) || 0,
      uvi: data?.daily[i].uvi || 0,
      weatherDescription: data?.daily[i].weather[0].description,
      weatherId: data?.daily[i].weather[0].id,
      windSpeed: Math.round(Number(data?.daily[i].wind_speed)) || 0,
    });

    currentDate.setDate(currentDate.getDate() + 1);
  }
  return { weekForecast, isLoading };
};
