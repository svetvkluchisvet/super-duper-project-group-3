import { FC, memo, PropsWithChildren, useState } from "react";
import ModalContainer from "../modal/modal-container";
import styles from "./WeekForecastCard.module.css";
import { useDailyForecastData } from "./use-daily-forecast-data";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowDownLong,
  faCloud,
  faDroplet,
  faSun,
  faUmbrella,
  faWind,
} from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import ReactTooltip from "react-tooltip";
import MetricDescription from "../metric-description/MetricDescription";
import { Skeleton } from "antd";
import {
  AppDataContextModel,
  useAppDataContext,
} from "../../context/app-data.provider";
import { AiOutlineClose } from "react-icons/ai";

interface WeekForecastCardProps {
  dayIndex: number;
}

const WeekForecastCard: FC<PropsWithChildren<WeekForecastCardProps>> = ({
  dayIndex,
}) => {
  const { t } = useTranslation();
  const { weekForecast, isLoading } = useDailyForecastData();
  const [modalOpen, setModalOpen] = useState(false);

  const { darkMode } = useAppDataContext() as AppDataContextModel;

  return (
    <>
      <div className={styles.card} onClick={() => setModalOpen(true)}>
        <p>{weekForecast[dayIndex].dt}</p>
        {isLoading && <Skeleton.Avatar size={100} />}
        <img
          src={`http://openweathermap.org/img/wn/${weekForecast[dayIndex].icon}@2x.png`}
          alt=""
          className={styles.icon}
        />
        {isLoading && <Skeleton paragraph={{ rows: 2 }} />}
        {!isLoading && [
          <p className={styles.temp}>{weekForecast[dayIndex].temp}°C</p>,
          <MetricDescription
            metricName={t("week_forecast_card.feels_like_title")}
            metricValue={`${weekForecast[dayIndex].feelsLike}°C`}
            type="main"
          />,
          <MetricDescription
            metricName={t("week_forecast_card.wind_title")}
            metricValue={`${weekForecast[dayIndex].windSpeed} ${t(
              "week_forecast_card.wind_units_of_measurement"
            )}`}
            type="main"
          />,
        ]}
      </div>
      {modalOpen && (
        <ModalContainer onClose={() => setModalOpen(false)}>
          <div className={darkMode ? "dark" : "light"}>
            <div className={styles.modalContent}>
              <div className={styles.firstBlock}>
                <p className={styles.extraText}>{weekForecast[dayIndex].dt}</p>
                <div className={styles.f}>
                  <div className={styles.tempBlock}>
                    <p className={styles.temp}>
                      {weekForecast[dayIndex].temp}°C
                    </p>
                    <MetricDescription
                      metricName={t("week_forecast_card.feels_like_title")}
                      metricValue={`${weekForecast[dayIndex].feelsLike}°C`}
                      type="main"
                    />
                  </div>
                  <img
                    src={`http://openweathermap.org/img/wn/${weekForecast[dayIndex].icon}@2x.png`}
                    alt="weather icon"
                    className={styles.icon}
                  />
                </div>
                <MetricDescription
                  metricName={t(`week_forecast_card.${weekForecast[dayIndex].mainWeatherType}`)}
                  metricValue={`${weekForecast[dayIndex].weatherDescription}`}
                  type="medium"
                />
              </div>
              <div className={styles.secondBlock}>
                <div className={styles.mainText} data-tip data-for="wind">
                  <FontAwesomeIcon icon={faWind} />
                  {weekForecast[dayIndex].windSpeed}
                  {t("week_forecast_card.wind_units_of_measurement")}
                </div>
                <ReactTooltip id="wind" place="left">
                  <span className={styles.tooltipText}>
                    {t("week_forecast_card.wind_title")}
                  </span>
                </ReactTooltip>

                <div className={styles.mainText} data-tip data-for="rain">
                  <FontAwesomeIcon icon={faDroplet} />
                  {weekForecast[dayIndex].rain}
                  {t("week_forecast_card.rain_units_of_measurement")}
                </div>
                <ReactTooltip id="rain" place="left">
                  <span className={styles.tooltipText}>
                    {t("week_forecast_card.rain_title")}
                  </span>
                </ReactTooltip>

                <div className={styles.mainText} data-tip data-for="pop">
                  <FontAwesomeIcon icon={faUmbrella} />
                  {weekForecast[dayIndex].probabilityOfPrecipitation}%
                </div>
                <ReactTooltip id="pop" place="left">
                  <span className={styles.tooltipText}>
                    {t("week_forecast_card.probability_of_precipitation_title")}
                  </span>
                </ReactTooltip>

                <div className={styles.mainText} data-tip data-for="clouds">
                  <FontAwesomeIcon icon={faCloud} />{" "}
                  {weekForecast[dayIndex].clouds}%
                </div>
                <ReactTooltip id="clouds" place="left">
                  <span className={styles.tooltipText}>
                    {t("week_forecast_card.clouds_title")}
                  </span>
                </ReactTooltip>

                <div className={styles.mainText} data-tip data-for="pressure">
                  <FontAwesomeIcon icon={faArrowDownLong} />
                  {weekForecast[dayIndex].pressure}
                  {t("week_forecast_card.pressure_units_of_measurement")}
                </div>
                <ReactTooltip id="pressure" place="left">
                  <span className={styles.tooltipText}>
                    {t("week_forecast_card.pressure_title")}
                  </span>
                </ReactTooltip>

                <div className={styles.mainText} data-tip data-for="uviIndex">
                  <FontAwesomeIcon icon={faSun} /> {weekForecast[dayIndex].uvi}
                </div>
                <ReactTooltip id="uviIndex" place="left">
                  <span className={styles.tooltipText}>
                    {t("week_forecast_card.uvi_index_title")}
                  </span>
                </ReactTooltip>
              </div>
              <div>
                <AiOutlineClose
                  className={styles.iconClose}
                  onClick={() => setModalOpen(false)}
                />
              </div>
            </div>
          </div>
        </ModalContainer>
      )}
    </>
  );
};

export default memo(WeekForecastCard);
