import { FC, memo } from "react";
import styles from "./AirPollutionPageContent.module.css";
import { useAirPollutionData } from "./use-air-pollution-data";
import ReactTooltip from "react-tooltip";
import { useTranslation } from "react-i18next";


const AirPollutionPageContent: FC = () => {
  const { t } = useTranslation();

  const { airPollutionContent } = useAirPollutionData();
  const airPoll = airPollutionContent.airQualityIndex;
  const co = Math.round(Number(airPollutionContent?.co));
  const no = Math.round(Number(airPollutionContent?.no));
  const no2 = Math.round(Number(airPollutionContent.no2));
  const o3 = Math.round(Number(airPollutionContent.o3));
  const so2 = Math.round(Number(airPollutionContent.so2));
  const pm2_5 = Math.round(Number(airPollutionContent.pm2_5));
  const pm10 = Math.round(Number(airPollutionContent.pm10));
  const nh3 = Math.round(Number(airPollutionContent.nh3));

  let coIndex;
  if (co >= 0 && co <= 50) coIndex = "good_condition";
  else if (co >= 50 && co <= 100) coIndex = "satisfactory_condition";
  else if (co >= 101 && co <= 200) coIndex = "moderate_condition";
  else if (co >= 201.0 && co <= 300) coIndex = "poor_condition";
  else if (co >= 301 && co <= 400) coIndex = "severe_condition";
  else if (co > 401) coIndex = "hazardous_condition";

  let noIndex;
  if (no >= 0 && no <= 50) noIndex = "good_condition";
  else if (no >= 50 && no <= 100) noIndex = "satisfactory_condition";
  else if (no >= 101 && no <= 200) noIndex = "moderate_condition";
  else if (no >= 201.0 && no <= 300) noIndex = "poor_condition";
  else if (no >= 301 && no <= 400) noIndex = "severe_condition";
  else if (no > 401) noIndex = "hazardous_condition";

  let noTwoIndex;
  if (no2 >= 0 && no2 <= 50) noTwoIndex = "good_condition";
  else if (no2 >= 50 && no2 <= 100) noTwoIndex = "satisfactory_condition";
  else if (no2 >= 101 && no2 <= 200) noTwoIndex = "moderate_condition";
  else if (no2 >= 201.0 && no2 <= 300) noTwoIndex = "poor_condition";
  else if (no2 >= 301 && no2 <= 400) noTwoIndex = "severe_condition";
  else if (no2 > 401) noTwoIndex = "hazardous_condition";

  let oThreeIndex;
  if (o3 >= 0 && o3 <= 50) oThreeIndex = "good_condition";
  else if (o3 >= 50 && o3 <= 100) oThreeIndex = "satisfactory_condition";
  else if (o3 >= 101 && o3 <= 200) oThreeIndex = "moderate_condition";
  else if (o3 >= 201.0 && o3 <= 300) oThreeIndex = "poor_condition";
  else if (o3 >= 301 && o3 <= 400) oThreeIndex = "severe_condition";
  else if (o3 > 401) oThreeIndex = "hazardous_condition";

  let soTwoIndex;
  if (so2 >= 0 && so2 <= 40) soTwoIndex = "good_condition";
  else if (so2 >= 41 && so2 <= 80) soTwoIndex = "satisfactory_condition";
  else if (so2 >= 81 && so2 <= 380) soTwoIndex = "moderate_condition";
  else if (so2 >= 381.0 && so2 <= 800) soTwoIndex = "poor_condition";
  else if (so2 >= 801 && so2 <= 1600) soTwoIndex = "severe_condition";
  else if (so2 > 1600) soTwoIndex = "hazardous_condition";

  let PMTwoHalfIndex;
  if (pm2_5 >= 0 && pm2_5 <= 30) PMTwoHalfIndex = "good_condition";
  else if (pm2_5 >= 31 && pm2_5 <= 60)
    PMTwoHalfIndex = "satisfactory_condition";
  else if (pm2_5 >= 61 && pm2_5 <= 90) PMTwoHalfIndex = "moderate_condition";
  else if (pm2_5 >= 91.0 && pm2_5 <= 120) PMTwoHalfIndex = "poor_condition";
  else if (pm2_5 >= 121 && pm2_5 <= 250) PMTwoHalfIndex = "severe_condition";
  else if (pm2_5 > 250) PMTwoHalfIndex = "hazardous_condition";

  let PMTenIndex;
  if (pm10 >= 0 && pm10 <= 50) PMTenIndex = "good_condition";
  else if (pm10 >= 51 && pm10 <= 100) PMTenIndex = "satisfactory_condition";
  else if (pm10 >= 101 && pm10 <= 250) PMTenIndex = "moderate_condition";
  else if (pm10 >= 251 && pm10 <= 350) PMTenIndex = "poor_condition";
  else if (pm10 >= 351 && pm10 <= 430) PMTenIndex = "severe_condition";
  else if (pm10 > 430) PMTenIndex = "hazardous_condition";

  let nhThreeIndex;
  if (nh3 >= 0 && nh3 <= 200) nhThreeIndex = "good_condition";
  else if (nh3 >= 201 && nh3 <= 400) nhThreeIndex = "satisfactory_condition";
  else if (nh3 >= 401 && nh3 <= 800) nhThreeIndex = "moderate_condition";
  else if (nh3 >= 801 && nh3 <= 1200) nhThreeIndex = "poor_condition";
  else if (nh3 >= 1201 && nh3 <= 1800) nhThreeIndex = "severe_condition";
  else if (nh3 > 1800) nhThreeIndex = "hazardous_condition";

  let aqi;
  if (airPoll === 1) {
    aqi = "good";
  } else if (airPoll === 2) {
    aqi = "fair";
  } else if (airPoll === 3) {
    aqi = "moderate";
  } else if (airPoll === 4) {
    aqi = "poor";
  } else if (airPoll === 5) {
    aqi = "very_poor";
  }

  return (
    <div className={styles.grid}>
      <div className={styles.item1}>
        <div className={styles.text}>
          <h1 className={styles.title}>
            {t("air_pollution_content.air_pollution_title")}
          </h1>
          <h3 className={styles.condition}>
            {t("air_pollution_content.air_pollution_subtitle")}:{" "}
            {t(`air_pollution_content.${aqi}`)}!
          </h3>
          {/*{airPollutionContent.airQualityIndex}*/}
          <section className={styles.description}>
            {t("air_pollution_content.air_pollution_description")}
          </section>
        </div>
      </div>

      <div data-tip data-for="ozone" className={styles.item2}>
        <div className={styles.him}>
          O<sub>3</sub>
        </div>
      </div>
      <ReactTooltip place="top" id="ozone" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.ozone_title")}: {airPollutionContent.o3}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${oThreeIndex}`)}
          </p>
          <p className={styles.elementDescription}>
            {t("air_pollution_content.ozone_description")}
          </p>
        </span>
      </ReactTooltip>

      <div data-tip data-for="pm25" className={styles.item3}>
        <div className={styles.him}>
          PM<sub>2.5</sub>
        </div>
      </div>
      <ReactTooltip place="top" id="pm25" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.fine_particles_title")}:{" "}
            {airPollutionContent.pm2_5}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${PMTwoHalfIndex}`)}
          </p>
          <p>{t("air_pollution_content.fine_particles_description")}</p>
        </span>
      </ReactTooltip>
      {/*href="https://en.wikipedia.org/wiki/Particulates"*/}

      <div data-tip data-for="pm10" className={styles.item4}>
        <div className={styles.him}>
          PM<sub>10</sub>
        </div>
      </div>
      <ReactTooltip place="bottom" id="pm10" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.coarse_particulate_title")}:{" "}
            {airPollutionContent.pm10}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${PMTenIndex}`)}
          </p>
          <p>{t("air_pollution_content.coarse_particulate_description")}</p>
        </span>
      </ReactTooltip>
      {/*     href="https://en.wikipedia.org/wiki/Particulates#Size,_shape_and_solubility_matter"*/}

      <div data-tip data-for="co" className={styles.item5}>
        <div className={styles.him}>CO</div>
      </div>
      <ReactTooltip place="top" id="co" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.carbon_monoxide_title")}:{" "}
            {airPollutionContent.co}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${coIndex}`)}
          </p>
          <p>{t("air_pollution_content.carbon_monoxide_description")}</p>
        </span>
      </ReactTooltip>
      {/*href="https://en.wikipedia.org/wiki/Carbon_monoxide"*/}

      <div data-tip data-for="no" className={styles.item6}>
        <div className={styles.him}>NO</div>
      </div>
      <ReactTooltip place="top" id="no" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.nitrogen_monooxide_title")}:{" "}
            {airPollutionContent.no}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${noIndex}`)}
          </p>
          <p>{t("air_pollution_content.nitrogen_monooxide_description")}</p>
        </span>
      </ReactTooltip>
      {/*    href="https://en.wikipedia.org/wiki/Nitric_oxide"*/}

      <div data-tip data-for="no2" className={styles.item7}>
        <div className={styles.him}>
          NO<sub>2</sub>
        </div>
      </div>
      <ReactTooltip place="top" id="no2" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.nitrogen_dioxide_title")}:{" "}
            {airPollutionContent.no2}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${noTwoIndex}`)}
          </p>
          <p>{t("air_pollution_content.nitrogen_dioxide_description")}</p>
        </span>
      </ReactTooltip>
      {/*    href="https://en.wikipedia.org/wiki/Nitrogen_dioxide"*/}

      <div data-tip data-for="so2" className={styles.item8}>
        <div className={styles.him}>
          SO<sub>2</sub>
        </div>
      </div>
      <ReactTooltip place="top" id="so2" type="light">
        <span className={styles.tooltiptext}>
          <p className={styles.element}>
            {t("air_pollution_content.sulphur_dioxide_title")}:{" "}
            {airPollutionContent.so2}
          </p>
          <p className={styles.condition}>
            {t(`air_pollution_content.${soTwoIndex}`)}
          </p>

          <p>{t("air_pollution_content.sulphur_dioxide_description")}</p>
        </span>
      </ReactTooltip>
      {/*    href="https://en.wikipedia.org/wiki/Sulfur_dioxide"*/}
      <div data-tip data-for="nh3" className={styles.item9}>
        <div className={styles.him}>
          NH<sub>3</sub>
        </div>
      </div>
      <div className={styles.tooltip}>
        <ReactTooltip place="left" id="nh3" type="light">
          <span className={styles.tooltiptext}>
            <p className={styles.element}>
              {" "}
              {t("air_pollution_content.ammonia_title")}:{" "}
              {airPollutionContent.nh3}
            </p>
            <p className={styles.condition}>
              {t(`air_pollution_content.${nhThreeIndex}`)}
            </p>
            <p>{t("air_pollution_content.ammonia_description")}</p>
          </span>
        </ReactTooltip>
      </div>
      {/*    href="https://en.wikipedia.org/wiki/Ammonia"*/}
    </div>
  );
};

export default memo(AirPollutionPageContent);
