import { useSelector } from "react-redux";
import { RootState } from "../../app/store";
import {useGetAirConditionQuery} from "../../features/airConditionApi/airConditionApi";

export type AirPollutionData = {
    co?: number; //Концентрация СО ( окиси углерода ), мкг/м^3
    no?: number;//Концентрация NO ( азота монооксида ), мкг/м^3
    no2?: number;//Концентрация NO2 ( двуокись азота ), мкг/м^3
    o3?: number;//Концентрация O3 ( озона ), мкг/м^3
    so2?: number;//Концентрация SO2 ( сернистого газа ), мкг/м^3
    pm2_5?: number;//Концентрация РМ 2,5 (мелкодисперсное вещество), мкг/м^3
    pm10?: number; //Концентрация РМ 10 ( крупнодисперсных частиц ), мкг/м 3
    nh3?: number; //Концентрация NH 3 ( аммиака ), мкг/м 3
    airQualityIndex?: number;
}

export const useAirPollutionData = (): { airPollutionContent: AirPollutionData, isLoading: boolean } => {
    const lat = useSelector((state: RootState) => state.locationSlice.lat);
    const lon = useSelector((state: RootState) => state.locationSlice.lon);
    const {data, isLoading} = useGetAirConditionQuery({lat, lon});
    console.log(data);
    const airPollutionContent = {
        co: data?.list[0].components.co,
        no: data?.list[0].components.no,
        no2: data?.list[0].components.no2,
        o3: data?.list[0].components.o3,
        so2: data?.list[0].components.so2,
        pm2_5: data?.list[0].components.pm2_5,
        pm10: data?.list[0].components.pm10,
        nh3: data?.list[0].components.nh3,
        airQualityIndex: data?.list[0].main.aqi,

    }

    return {airPollutionContent, isLoading};
}
