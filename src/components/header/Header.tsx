import { Link } from "react-router-dom";
import LocationSelect from "../location-select/location-select";
import Logo from "../logo/Logo";
import styles from "./Header.module.css";
import { useTranslation } from "react-i18next";
import { FC, memo } from "react";
import "antd/dist/antd.css";

const Header: FC = () => {
  const { t } = useTranslation();
  return (
    <header className={styles.header}>
        <Link to={"/"}>
            <Logo />
        </Link>
      <div className={styles.headerContent}>
        <Link className={styles.bud} to={"/air-pollution"}>
          <span className={styles.link}>{t("nav.air_condition_title")}</span>
        </Link>
        <LocationSelect />
      </div>
    </header>
  );
};

export default memo(Header);
