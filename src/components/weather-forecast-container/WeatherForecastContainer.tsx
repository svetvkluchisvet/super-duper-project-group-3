import { FC, memo, PropsWithChildren } from "react"
import styles from "./WeatherForecastContainer.module.css";

interface WeatherForecastContainerProps {}

const WeatherForecastContainer: FC<PropsWithChildren<WeatherForecastContainerProps>> = ({children}) => {
  return (
    <div className={styles.forecast}>{children}</div>
  )
}

export default memo(WeatherForecastContainer);
