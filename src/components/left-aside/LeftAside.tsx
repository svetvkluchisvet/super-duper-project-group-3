import {FC, memo, PropsWithChildren} from 'react';
import styles from './LeftAside.module.css';

interface LeftAsideProps {
}

const LeftAside: FC<PropsWithChildren<LeftAsideProps>> = ({children}) => {
    return (
        <aside className={styles.aside}>{children}</aside>
    );
}

export default memo(LeftAside);
