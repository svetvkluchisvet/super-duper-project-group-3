import { FC, memo, PropsWithChildren } from "react";
import styles from "./DailyForecastCard.module.css";
import { useDailyForecastData } from "./use-daily-forecast-data";
import cn from "classnames";
import { useTranslation } from "react-i18next";
import MetricDescription from "../metric-description/MetricDescription";
import { Skeleton } from "antd";


interface DailyForecastCardProps {
  timeOfDayIndex: number;
  type?: 'morning' | 'afternoon' | 'evening' | 'night';
}

const timeOfDayList = ['morning', 'afternoon', 'evening', 'night'];

const DailyForecastCard: FC<PropsWithChildren<DailyForecastCardProps>> = ({
  timeOfDayIndex,
  type = 'afternoon'
}) => {
  const { t } = useTranslation();
  const { dailyForecast, isLoading } = useDailyForecastData();

  return (
    <div className={cn(styles.card, styles[type])} style={styles}>
      <p>{t(`time_of_day_list.${timeOfDayList[timeOfDayIndex]}`)}</p>
      {isLoading && <Skeleton.Avatar size={100}/>}<img src={`http://openweathermap.org/img/wn/${dailyForecast[timeOfDayIndex].icon}@2x.png`} alt="" className={styles.icon}/>
      {isLoading && <Skeleton paragraph={{ rows: 3}}/>}
      {!isLoading && [
      <p className={styles.temp}>{dailyForecast[timeOfDayIndex].temp}°C</p>,
      <MetricDescription metricName={t('daily_forecast_card.feels_like_title')} metricValue={`${dailyForecast[timeOfDayIndex].feelsLike}°C`} type="main"/>,
      <MetricDescription metricName={t('daily_forecast_card.wind_title')} metricValue={`${dailyForecast[timeOfDayIndex].windSpeed} ${t('daily_forecast_card.wind_units_of_measurement')}`} type="main"/>,
      <MetricDescription metricName={t('daily_forecast_card.rain_title')} metricValue={`${dailyForecast[timeOfDayIndex].rain} ${t('daily_forecast_card.rain_units_of_measurement')}`} type="main"/>
      ]}
    </div>
    )
  }

export default memo(DailyForecastCard);
