import { useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { AppDataContextModel, useAppDataContext } from "../../context/app-data.provider";
import { useGetCurrentForecastQuery } from "../../features/currentForecastApi/currentForecastApi";
import { locationSliceSelectors } from "../../features/geocodingSlice/geocodingSlice";
import { useTypedSelector } from "../../features/selector/selector";
import { useAppSelector } from "../../app/hooks";
import { appDataSelectors } from "../../features/app-data/app-data.slice";

export type DailyForecastData = {
  temp?: number;
  feelsLike?: number;
  windSpeed?: number;
  rain?: number;
  weatherId?: number;
  mainWeatherType?: string;
  weatherDescription?: string;
  icon?: string;
  pressure?: number;
  clouds?: number;
  probabilityOfPrecipitation?: number;
  uvi?: number;
};

export const useDailyForecastData = (): {
  dailyForecast: DailyForecastData[];
  isLoading: boolean;
} => {
  const lat = useSelector((state: RootState) => state.locationSlice.lat);
  const lon = useSelector((state: RootState) => state.locationSlice.lon);
  const { language } = useAppDataContext() as AppDataContextModel;

  const { data, isLoading } = useGetCurrentForecastQuery({
    lat,
    lon,
    language,
  });

  const morningForecast = {
    temp: Math.round(Number(data?.daily[0].temp.morn)),
    feelsLike: Math.round(Number(data?.daily[0].feels_like.morn)),
    windSpeed: Math.round(Number(data?.daily[0].wind_speed)),
    rain: data?.daily[0].rain || 0,
    icon: data?.daily[0].weather[0].icon,
  };

  const dayForecast = {
    temp: Math.round(Number(data?.daily[0].temp.day)),
    feelsLike: Math.round(Number(data?.daily[0].feels_like.day)),
    windSpeed: Math.round(Number(data?.daily[0].wind_speed)),
    rain: data?.daily[0].rain || 0,
    icon: data?.daily[0].weather[0].icon,
  };

  const eveningForecast = {
    temp: Math.round(Number(data?.daily[0].temp.eve)),
    feelsLike: Math.round(Number(data?.daily[0].feels_like.eve)),
    windSpeed: Math.round(Number(data?.daily[0].wind_speed)),
    rain: data?.daily[0].rain || 0,
    icon: data?.daily[0].weather[0].icon,
  };

  const nightForecast = {
    temp: Math.round(Number(data?.daily[0].temp.night)),
    feelsLike: Math.round(Number(data?.daily[0].feels_like.night)),
    windSpeed: Math.round(Number(data?.daily[0].wind_speed)),
    rain: data?.daily[0].rain || 0,
    icon: data?.daily[0].weather[0].icon,
  };

  const dailyForecast = [
    morningForecast,
    dayForecast,
    eveningForecast,
    nightForecast,
  ];

  return { dailyForecast, isLoading };
};
