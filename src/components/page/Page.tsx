import { FC, memo, PropsWithChildren } from "react";
import ButtonsContainer from "../buttons-container/ButtonsContainer";
import Header from "../header/Header";
import LanguageButton from "../language-button/LanguageButton";
import LeftAside from "../left-aside/LeftAside";
import MainWidget from "../main-widget/MainWidget";
import Main from "../main/Main";
import RightAside from "../right-aside/RightAside";
import styles from "./Page.module.css";
import ThemeButton from "../theme-button/ThemeButton";
import Media from "react-media";

const Page: FC<PropsWithChildren<{}>> = ({children}) => {
    return (
        <Media query={{ maxWidth: 599 }}>
            {matches =>
                matches ? (
                    <div className={styles.pagesmall}>
                        <Header/>
                        <LeftAside>
                            <ButtonsContainer type="lower">
                                <LanguageButton/>
                                <ThemeButton/>
                            </ButtonsContainer>
                        </LeftAside>
                        <RightAside>
                            <MainWidget/>
                        </RightAside>
                        <Main>{children}</Main>
                    </div>
                ) : (
                    <div className={styles.page}>
                        <Header/>
                        <LeftAside>
                            <ButtonsContainer type="lower">
                                <LanguageButton/>
                                <ThemeButton/>
                            </ButtonsContainer>
                        </LeftAside>
                        <Main>{children}</Main>
                        <RightAside>
                            <MainWidget/>
                        </RightAside>
                    </div>
                )
            }
        </Media>
    );
};

export default memo(Page);
