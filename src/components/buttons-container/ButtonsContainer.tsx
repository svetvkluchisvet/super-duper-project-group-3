import { FC, memo, PropsWithChildren } from "react";
import styles from './ButtonsContainer.module.css';
import cn from 'classnames';

interface ButtonsContainerProps {
  type?: 'upper' | 'lower';
}

const ButtonsContainer: FC<PropsWithChildren<ButtonsContainerProps>> = ({
  children,
  type = 'lower',
}) => {
  return (
    <div className={cn(styles.buttonsContainer, styles[type])} style={styles}>
      {children}
    </div>
  );
}

export default memo(ButtonsContainer);
