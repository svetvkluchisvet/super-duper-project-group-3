import { FC, memo, PropsWithChildren, ReactNode } from "react";
import styles from "./card.module.css";

interface CardComponentProps {
  title?: ReactNode;
}

const CardComponent: FC<PropsWithChildren<CardComponentProps>> = ({
  children,
  title,
}) => {
  return (
    <div className={styles.card}>
      {title && <div className={styles.title}>{title}</div>}
      <>{children}</>
    </div>
  );
};

export default memo(CardComponent);
