import { FC, memo, PropsWithChildren } from "react";
import styles from "./MetricDescription.module.css";

interface MetricDescriptionProps {
  metricName: string;
  metricValue: string | number;
  type?: string;
}

const MetricDescription: FC<PropsWithChildren<MetricDescriptionProps>> = ({
  metricName,
  metricValue,
  type = "medium"
}) => {
  return (
    <p className={styles[type]}>
      <span className={styles.metricName}>{metricName}:</span> <span className={styles.metricValue}>{metricValue}</span>
    </p>
  )
}

export default memo(MetricDescription);
