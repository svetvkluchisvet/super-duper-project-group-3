import {FC, memo} from 'react';
import styles from './LogoPic.module.css';

const Logo: FC = () => {
    return (
            <div className={styles.logo}></div>
    )
};

export default memo(Logo);
