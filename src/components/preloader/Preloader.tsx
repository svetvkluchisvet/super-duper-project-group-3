import { FC, memo } from "react";
import styles from "./Preloader.module.css";

const Preloader: FC = () => {
  return (<div className={styles.ldsDualRing} id="preloader"></div>)
}

export default memo(Preloader);
