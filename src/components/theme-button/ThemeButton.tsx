import { FC, memo } from "react";
import styles from "./ThemeButton.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import {
  AppDataContextModel,
  useAppDataContext,
} from "../../context/app-data.provider";


const ThemeButton: FC = () => {
  const { darkMode, setDarkMode } = useAppDataContext() as AppDataContextModel;

  const icon = darkMode ? (
    <FontAwesomeIcon icon={faSun} className={styles.sun} />
  ) : (
    <FontAwesomeIcon icon={faMoon} className={styles.moon} />
  );

  return (
    <button
      className={styles.theme}
      onClick={() => {
        setDarkMode(!darkMode);
        const theme = darkMode ? "dark" : "light";
        localStorage.setItem("theme", theme);
      }}
    >
      {icon}
    </button>
  );
};

export default memo(ThemeButton);
