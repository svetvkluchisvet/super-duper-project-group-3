import { FC, memo } from "react";
import WeekForecastCard from "../week-forecast-card/WeekForecastCard";

interface WeekForecastProps {}

const days = [0, 1, 2, 3, 4, 5, 6, 7];
const WeekForecast: FC<WeekForecastProps> = () => {
  return (
    <>
      {days.map((it) => (
        <WeekForecastCard dayIndex={it} />
      ))}
    </>
  );
};

export default memo(WeekForecast);
