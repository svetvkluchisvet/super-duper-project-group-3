import { FC, memo, PropsWithChildren } from "react";
import ReactDOM from "react-dom";
import classNames from "./modal-container.module.css";

type ModalContainerProps = {
  onClose: () => void;
};
const ModalContainer: FC<PropsWithChildren<ModalContainerProps>> = ({
  children,
  onClose,
}) => {
  return ReactDOM.createPortal(
    <>
      <div className={classNames.shadow} onClick={onClose} />
      <div className={classNames.modal}>
      <div className={classNames.content}>{children}</div>
      </div>
    </>,
    document.getElementById("app-modal") as HTMLDivElement
  );
};

export default memo(ModalContainer);
