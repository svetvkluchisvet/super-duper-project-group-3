import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {CoordRoot} from "../../types/air-components-api.types";
import { key } from "../apiKey";


export const airConditionApi = createApi({
    reducerPath: "airConditionApi",
    baseQuery: fetchBaseQuery({baseUrl: "http://api.openweathermap.org/data/2.5"}),
    endpoints: build => ({
        getAirCondition: build.query<CoordRoot, { lat: number, lon: number }>({
            query: (arg) => {
                const {lat, lon} = arg;
                return {
                    url: `air_pollution?lat=${lat}&lon=${lon}&appid=${key}`,
                    params: {lat, lon},
                };
            },
        }),
    }),
});

export const {useGetAirConditionQuery} = airConditionApi;
