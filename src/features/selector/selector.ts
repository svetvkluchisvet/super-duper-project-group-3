import { createDraftSafeSelector, createSelector } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { LocationSliceState } from "../geocodingSlice/geocodingSlice";




export const selectLss = (state: LocationSliceState) => state;
export const lonSelect = createSelector(selectLss, (state) => state.lon)
export const latSelector = createDraftSafeSelector(
  selectLss,
  (state) => state.lon
);

export const useTypedSelector: TypedUseSelectorHook<LocationSliceState> =
  useSelector;
