import {
  createDraftSafeSelector,
  createSelector,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export interface LocationSliceState {
  city?: string;
  lat: number;
  lon: number;
}

const initialState: LocationSliceState = {
  city: "",
  lat: 0,
  lon: 0,
};

export const locationSlice = createSlice({
  name: "locationSlice",
  initialState,
  reducers: {
    setLocationData: (state, action: PayloadAction<LocationSliceState>) => {
      state.city = action.payload.city;
      state.lat = action.payload.lat;
      state.lon = action.payload.lon;
    },
  },
});

export const locationSliceActions = locationSlice.actions;
const selectLocationSliceState = (state: RootState) => state.locationSlice;

const selecCityName = createSelector(
  selectLocationSliceState,
  (state) => (state.lon, state.lat)
);

const selectCity = createDraftSafeSelector(
  selectLocationSliceState,
  (state) => state.city
);
export const locationSliceSelectors = {
  selectCity,
  selectLocationSliceState,
  selecCityName,
};
