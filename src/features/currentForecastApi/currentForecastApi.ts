import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { ForecastObject } from "../../types/current-forecast-weather-api.types";
import { key } from "../apiKey";


export const currentForecastApi = createApi({
  reducerPath: "currentForecastApi",
  baseQuery: fetchBaseQuery({baseUrl:"https://api.openweathermap.org/data/2.5"}),
  endpoints: build =>({
    getCurrentForecast: build.query<ForecastObject , { lat: number; lon: number; language: string }>({
      query: (arg) => {
        const {lat, lon, language} = arg;
        return {
          url: `onecall?lat=${lat}&lon=${lon}&appid=${key}&units=metric&lang=${language}`,
          params: arg,
        };
      },
    }),
  }),
});

export const { useGetCurrentForecastQuery } = currentForecastApi;
