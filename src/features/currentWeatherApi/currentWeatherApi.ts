import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import { useState } from "react";
import { CurrentWeatherObjects } from "../../types/current-weather-api.types";
import { key } from "../apiKey";


export const currentWeatherApi = createApi({
    reducerPath: "currentWeatherApi",
    baseQuery: fetchBaseQuery({baseUrl:"https://api.openweathermap.org/data/2.5"}),
    endpoints: build =>({
        getCurrentWeather: build.query<CurrentWeatherObjects, {lat: number, lon: number, language: string}>({
            query: (arg) => {
                const { lat, lon, language } = arg;
                
                return {
                    url: `weather?lat=${lat}&lon=${lon}&appid=${key}&units=metric&lang=${language}`,
                    params: { lat, lon },
                };
            },
        }),
    }),
});

export const {useGetCurrentWeatherQuery} = currentWeatherApi;
