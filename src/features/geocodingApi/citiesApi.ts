import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { CitiesApiResponse } from "../../types/cities-api.types";

export type CitiesApiParams = {
  namePrefix: string;
  limit?: number;
  [k: string]: any;
};
export const citiesApi = createApi({
  reducerPath: "citiesApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://wft-geo-db.p.rapidapi.com/v1/geo",
    headers: {
      "X-RapidAPI-Host": "wft-geo-db.p.rapidapi.com",
      "X-RapidAPI-Key": "98a8603711msh2e93356f173d489p119afejsnd59ac0d1745b",
    },
  }),
  endpoints: (build) => ({
    getCities: build.query<CitiesApiResponse, CitiesApiParams>({
      query: ({ limit, namePrefix }) => {
        return {
          url: `/cities`,
          params: {
            limit: limit ?? 10,
            namePrefix,
            sort: "-population",
          },
          headers: {
            "X-RapidAPI-Host": "wft-geo-db.p.rapidapi.com",
            "X-RapidAPI-Key":
              "98a8603711msh2e93356f173d489p119afejsnd59ac0d1745b",
          },
        };
      },
    }),
  }),
});

export const { useGetCitiesQuery, useLazyGetCitiesQuery } = citiesApi;
