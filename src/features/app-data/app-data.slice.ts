import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface AppDataState {
  language: string;
}

export const appDataInitialState: AppDataState = {
  language: "en",
};

const appDataSlice = createSlice({
  initialState: appDataInitialState,
  name: "appData",
  reducers: {
    setLanguage: (state, action: PayloadAction<string>) => {
      state.language = action.payload;
    },
  },
});

const selectAppDataState = (state: RootState) => state.appData;

const selectLanguage = createSelector(
  selectAppDataState,

  (state) => state.language
);
export const appDataSelectors = {
  selectLanguage,
};

export const appDataReducer = appDataSlice.reducer;

export const appDataActions = {
  ...appDataSlice.actions,
};
