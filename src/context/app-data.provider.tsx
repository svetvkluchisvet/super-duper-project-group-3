import { createContext, FC, PropsWithChildren, useContext, useState } from 'react';

export type AppDataContextModel = {
  language: string;
  setLanguage: (l: string) => void;

  darkMode: boolean;
  setDarkMode: (darkMode: boolean) => void;
};
const AppDataContext = createContext<AppDataContextModel | undefined>(
  undefined
);


export const AppDataProvider: FC<PropsWithChildren<{}>> = ({ children }) => {
  const [language, setLanguage] = useState("en");

  const theme = localStorage.getItem('theme')
  const [darkMode, setDarkMode] = useState(theme === 'light');

  return (
    <AppDataContext.Provider value={{ language, setLanguage, darkMode, setDarkMode}}>
      {children}
    </AppDataContext.Provider>
  );
};

export const useAppDataContext = () => useContext(AppDataContext);
