import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import { appDataReducer } from "../features/app-data/app-data.slice";
import counterReducer from "../features/counter/counterSlice";
import { airConditionApi } from "../features/airConditionApi/airConditionApi";
import { currentWeatherApi } from "../features/currentWeatherApi/currentWeatherApi";
import { citiesApi } from "../features/geocodingApi/citiesApi";
import { currentForecastApi } from "../features/currentForecastApi/currentForecastApi";
import logger from "redux-logger";
import { locationSlice } from "../features/geocodingSlice/geocodingSlice";


export const store = configureStore({
  reducer: {
    counter: counterReducer,
    [currentWeatherApi.reducerPath]: currentWeatherApi.reducer,
    [airConditionApi.reducerPath]: airConditionApi.reducer,
    [currentForecastApi.reducerPath]: currentForecastApi.reducer,
    [citiesApi.reducerPath]: citiesApi.reducer,
    locationSlice: locationSlice.reducer,
    appData: appDataReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([logger]),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
