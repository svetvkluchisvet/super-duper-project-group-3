import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { store } from "./app/store";
import { Provider } from "react-redux";
import { AppDataProvider } from "./context/app-data.provider";
import * as serviceWorker from "./serviceWorker";
import "./i18next";
import PreloaderWithClouds from "./components/preloader-with-clouds/PreloaderWithClouds";
import "leaflet/dist/leaflet.css";



ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <AppDataProvider>
        <Suspense fallback={<PreloaderWithClouds />}>
          <App />
        </Suspense>
      </AppDataProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
