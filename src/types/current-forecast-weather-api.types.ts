export interface ForecastObject {
    lat: number
    lon: number
    timezone: string //Название часового пояса для запрошенного местоположения
    timezone_offset: number //Сдвиг в секундах от UTC
    current: CurrentWeather//Текущий ответ API данных о погоде
    minutely: MinutelyWeather[]//
    hourly: HourlyWeather[]
    daily: DailyWeather[]
    alerts: Alert[]
}

export interface CurrentWeather {
    dt: number
    sunrise: number
    sunset: number
    temp: number
    feels_like: number
    pressure: number
    humidity: number
    dew_point: number //точка росы
    uvi: number//уф-индекс
    clouds: number
    visibility: number
    wind_speed: number
    wind_deg: number
    weather: MainCurrentWeatherInfo[]
    rain: ForecastRain
}

export interface MainCurrentWeatherInfo {
    id: number
    main: string
    description: string
    icon: string
}

export interface ForecastRain {
    '1h': number
}

export interface MinutelyWeather {
    dt: number
    precipitation: number//объем осадков
}

export interface HourlyWeather {
    dt: number
    temp: number
    feels_like: number
    pressure: number
    humidity: number
    dew_point: number
    uvi: number
    clouds: number
    visibility: number
    wind_speed: number
    wind_deg: number
    wind_gust: number
    weather: MainHourlyWeatherInfo[]
    pop: number
}

export interface MainHourlyWeatherInfo {
    id: number
    main: string
    description: string
    icon: string
}

export interface DailyWeather {
    dt: number
    sunrise: number
    sunset: number
    moonrise: number
    moonset: number
    moon_phase: number
    temp: Temperature
    feels_like: FeelsLike
    pressure: number
    humidity: number
    dew_point: number
    wind_speed: number
    wind_deg: number
    weather: MainDailyWeatherInfo[]
    clouds: number
    pop: number
    rain: number
    uvi: number
}

export interface Temperature {
    day: number
    min: number
    max: number
    night: number
    eve: number
    morn: number
}

export interface FeelsLike {
    day: number
    night: number
    eve: number
    morn: number
}

export interface MainDailyWeatherInfo {
    id: number
    main: string
    description: string
    icon: string
}

export interface Alert {
    sender_name: string
    event: string
    start: number
    end: number
    description: string
    tags: string[] //тип (суровой серьезной...) погоды
}
