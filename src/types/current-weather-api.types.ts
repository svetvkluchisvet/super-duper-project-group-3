export interface Coord {
  longitude: number; //долгота
  latitude: number; //широта
}

export interface Weather {
  weatherId: number; //Идентификатор погодных условий
  weatherParam: string; //Группа погодных параметров (Дождь, Снег и т.д.)
  description: string; //??(уточнение состояния погоды)
  icon: string; // иконка
}

export interface MainCurrentWeatherCharacteristics {
  temp: number; //Температура
  feels_like: number; //ощущается как
  tempMin?: number; //Минимальная температура на данный момент. Это минимальная температура, наблюдаемая в настоящее время (в пределах крупных мегаполисов и городских территорий).
  tempMax?: number; //Максимальная температура на данный момент. Это максимальная наблюдаемая в настоящее время температура (в пределах крупных мегаполисов и городских территорий).
  pressure?: number; //давление
  humidity?: number; //влажность
  seaAtmosphericPressure?: number; //Атмосферное давление на уровне моря, гПа
  groundAtmosphericPressure?: number; //Атмосферное давление на уровне земли, гПа
  gust?: number; //Порыв ветра. Единица измерения по умолчанию: метр/сек,
}

export interface Wind {
  speed: number; //скорость ветраа
  deg: string; //направление
}

export interface Clouds {
  cloudiness: number; //Облачность, %
}

export interface Rain {
  rainVolume1h: number; //Объем дождя за последний 1 час, мм
  rainVolume3h: number; //Объем дождя за последний 3 час, мм
}

export interface SunInf {
  type: number;
  id: number;
  message: number;
  country: string; // код страны
  sunrise: number; // Время восхода солнца, unix, UTC
  sunset: number; // Время захода солнца, unix, UTC
}

export interface CurrentWeatherObjects {
  coord: Coord;
  weather: Weather[];
  base: string; //?
  main: MainCurrentWeatherCharacteristics;
  visibility: number; //Видимость, метр
  wind: Wind;
  clouds: Clouds;
  dataCalculationTime: number; //Время расчета данных, unix, UTC
  sys: SunInf;
  timezone: number; //Сдвиг в секундах от UTC
  cityId: number; //Идентификатор города
  name: string; //Название города
  cityCod: number; //Внутренний параметр???
}
