export interface CoordRoot {
    coord: number[];//Координаты из указанного места (широта, долгота)
    list: AirComponentsList[];
}

export interface AirComponentsList {
    dt: number; //Дата и время, Unix, UTC
    main: MainAirCondition
    components: AirComponents
}

export interface MainAirCondition {
    aqi: number; //Индекс качества воздуха. Возможные значения: 1, 2, 3, 4, 5. Где 1 = хорошо, 2 = удовлетворительно, 3 = умеренно, 4 = плохо, 5 = очень плохо.
}

export interface AirComponents {
    co: number; //Концентрация СО ( окиси углерода ), мкг/м^3
    no: number;//Концентрация NO ( азота монооксида ), мкг/м^3
    no2: number;//Концентрация NO2 ( двуокись азота ), мкг/м^3
    o3: number;//Концентрация O3 ( озона ), мкг/м^3
    so2: number;//Концентрация SO2 ( сернистого газа ), мкг/м^3
    pm2_5: number;//Концентрация РМ 2,5 (мелкодисперсное вещество), мкг/м^3
    pm10: number; //Концентрация РМ 10 ( крупнодисперсных частиц ), мкг/м 3
    nh3: number; //Концентрация NH 3 ( аммиака ), мкг/м 3
}