export interface CoordTown {
    place_id:     string;
    licence:      string;
    osm_type:     string;
    osm_id:       string;
    boundingbox:  string[];
    lat:          number;
    lon:          number;
    display_name: string;
    class:        string;
    type:         string;
    importance:   number;
    icon:         string;
    address?:      Address;
    extratags:    Extratags;
}

export interface Address {
    city:             string;
    state_district:   string;
    state:            string;
    "ISO3166-2-lvl4": string;
    postcode:         string;
    country:          string;
    country_code:     string;
}

export interface Extratags {
    capital?:    string;
    website?:    string;
    wikidata?:   string;
    wikipedia?:  string;
    population?: string;
}
