export interface CitiesApiResponse {
  data: CityInfo[];
  links: CityLink[];
  metadata: CityApiResponseMetadata;
}

export interface CityInfo {
  id: number;
  wikiDataId: string;
  type: string;
  city: string;
  name: string;
  country: string;
  countryCode: string;
  region: string;
  regionCode: string;
  latitude: number;
  longitude: number;
  population: number;
}

export interface CityLink {
  rel: string;
  href: string;
}

export interface CityApiResponseMetadata {
  currentOffset: number;
  totalCount: number;
}
